const rockButton = window.document.getElementById("RockButton");
const paperButton = window.document.getElementById("PaperButton");
const scissorsButton = window.document.getElementById("ScissorsButton");

rockButton.addEventListener("click",rock);
paperButton.addEventListener("click",paper);
scissorsButton.addEventListener("click",scissors);

// Rock = 1
// Paper = 2
// Scissors = 3

function rock() {
    
    
    numberRandom = Math.floor(Math.random() * (4 - 1) + 1);
    if (numberRandom === 1){
      
        document.getElementById("you").innerHTML = "YOU TIED!";
        document.getElementById("RockButton2").style["boxShadow"] = "inset 0 0 4em white, 0 0 5em black";
        document.getElementById("PaperButton2").style["boxShadow"] = "";
        document.getElementById("ScissorsButton2").style["boxShadow"] = "";


    }
    if (numberRandom == 2){
        document.getElementById("you").innerHTML = "YOU LOST!";
        document.getElementById("PaperButton2").style["boxShadow"] = "inset 0 0 4em white, 0 0 5em black";
        document.getElementById("ScissorsButton2").style["boxShadow"] = "";
        document.getElementById("RockButton2").style["boxShadow"] = "";
        
    }
    if (numberRandom == 3){
        document.getElementById("you").innerHTML = "YOU WON!";
        document.getElementById("ScissorsButton2").style["boxShadow"] = "inset 0 0 4em white, 0 0 5em black";
        document.getElementById("RockButton2").style["boxShadow"] = "";
        document.getElementById("PaperButton2").style["boxShadow"] = "";
    }
}

function paper() {
    numberRandom = Math.floor(Math.random() * (4 - 1) + 1);
    if (numberRandom == 1){
        
        document.getElementById("you").innerHTML = "YOU WON!";
        document.getElementById("RockButton2").style["boxShadow"] = "inset 0 0 4em white, 0 0 5em black";
        document.getElementById("PaperButton2").style["boxShadow"] = "";
        document.getElementById("ScissorsButton2").style["boxShadow"] = "";



    }
    if (numberRandom == 2){
        
        document.getElementById("you").innerHTML = "YOU TIED!";
        document.getElementById("PaperButton2").style["boxShadow"] = "inset 0 0 4em white, 0 0 5em black";
        document.getElementById("ScissorsButton2").style["boxShadow"] = "";
        document.getElementById("RockButton2").style["boxShadow"] = "";

    }
    if (numberRandom == 3){
        
        document.getElementById("you").innerHTML = "YOU LOST!";
        document.getElementById("ScissorsButton2").style["boxShadow"] = "inset 0 0 4em white, 0 0 5em black";
        document.getElementById("RockButton2").style["boxShadow"] = "";
        document.getElementById("PaperButton2").style["boxShadow"] = "";
    }

}

function scissors() {
    numberRandom = Math.floor(Math.random() * (4 - 1) + 1);
    if (numberRandom == 1){
       
        document.getElementById("you").innerHTML = "YOU LOST!";
        document.getElementById("RockButton2").style["boxShadow"] = "inset 0 0 4em white, 0 0 5em black";
        document.getElementById("PaperButton2").style["boxShadow"] = "";
        document.getElementById("ScissorsButton2").style["boxShadow"] = "";

    }
    if (numberRandom == 2){
        
        document.getElementById("you").innerHTML = "YOU WON!";
        document.getElementById("PaperButton2").style["boxShadow"] = "inset 0 0 4em white, 0 0 5em black";
        document.getElementById("ScissorsButton2").style["boxShadow"] = "";
        document.getElementById("RockButton2").style["boxShadow"] = "";

    }
    if (numberRandom == 3){
        
        document.getElementById("you").innerHTML = "YOU TIED!";
        document.getElementById("ScissorsButton2").style["boxShadow"] = "inset 0 0 4em white, 0 0 5em black";
        document.getElementById("RockButton2").style["boxShadow"] = "";
        document.getElementById("PaperButton2").style["boxShadow"] = "";

    }

}